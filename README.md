# GitLab CI/CD Components: Common

[Includeable](https://docs.gitlab.com/ee/ci/yaml/includes.html) [GitLab CI/CD](https://docs.gitlab.com/ee/topics/build_your_application.html) 
configuration for tasks common to all projects, regardless of language or platform.

---

<sub>Emoji used for repository logo designed by <a href="https://openmoji.org/">OpenMoji</a> – the open-source emoji and icon project. License: CC BY-SA 4.0</sub>

---
